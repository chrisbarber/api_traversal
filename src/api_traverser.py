from twisted.web.client import Agent, readBody, HTTPConnectionPool, \
    ResponseFailed, RequestGenerationFailed, RequestTransmissionFailed
from twisted.internet.error import ConnectError, DNSLookupError, \
    ConnectionLost
from twisted.web.error import SchemeNotSupported
import twisted.web.error
from twisted.internet import defer, task
from twisted.logger import Logger
from twisted.protocols.htb import Bucket
from twisted.internet.defer import inlineCallbacks, returnValue
import json
import numbers
import resource


class ApiTraverserError(RuntimeError):
    pass
class ApiTraverserRequestFailed(Exception):
    pass
class ApiTraverserInvalidResponse(Exception):
    pass
class ApiTraverserNonTreeException(Exception):
    pass


class ApiTraverser:
    """Class implementing asyncronous concurrent traversal and summation of
    rewards from a tree of JSON API's with a given response format."""

    log = Logger()

    #: bucket_add_per_request
    #: bucket_rate
    #: bucket_maxburst
    #:   Rate limiting configuration;
    #:   See ``twisted.protocols.htb.Bucket documentation``
    bucket_add_per_request = 1
    bucket_rate = 100
    bucket_maxburst = 20

    #: http_connection_timeout
    http_connection_timeout = 10.

    #: rx_limit
    #:   Despite rate limiting, file handles could still be exhausted;
    #:   default to a conservative limit half the current system limit
    rx_limit = resource.getrlimit(resource.RLIMIT_NOFILE)[0] // 2

    #: failed_reward
    #:   A reward value to assign to API requests that fail. If some retry
    #:   policy is needed, override ``_get_page``.
    failed_reward = None


    def __init__(self, reactor=None):
        """Create an instance of ``ApiTraverser``. See documented class
        variables for customization options."""

        if reactor is None:
            from twisted.internet import reactor
        self._reactor = reactor

        self.bucket = Bucket()
        self.bucket.rate = self.bucket_rate
        self.bucket.maxburst = self.bucket_maxburst

        self.limit_rx_semaphore = defer.DeferredSemaphore(self.rx_limit)

        self.http_pool = HTTPConnectionPool(self._reactor, persistent=True)
        self.http_pool.cachedConnectionTimeout = 60.
        self.agent = Agent(self._reactor, connectTimeout=self.http_connection_timeout, pool=self.http_pool)


    def _failed(self, failure):
        """Generic Errback which logs the failure, and leaves it unhandled."""

        self.log.error(failure.getErrorMessage())
        return failure


    @inlineCallbacks
    def _get_page(self, url):
        """Makes an HTTP GET to the given url and returns the response body as bytes."""

        try:
            d = self.limit_rx_semaphore.run(self.agent.request, b'GET', bytes(url, 'utf-8'))
            d.addCallback(readBody)
            result = yield d
            returnValue(result)
        except (ResponseFailed, RequestGenerationFailed, RequestTransmissionFailed, \
                twisted.web.error.Error, ConnectError, DNSLookupError, ConnectionLost, \
                SchemeNotSupported) as e:
            raise ApiTraverserRequestFailed('Failure getting page \'{0}\''.format(url)) from e
    

    @inlineCallbacks
    def _crawl(self, url):
        """Gets response from a JSON API at the given URL. Returns the parsed JSON."""

        try:
            self.log.debug('GET {0}'.format(url))
            result = yield self._get_page(url)

            if result == b'Invalid endpoint':
                raise ApiTraverserError('Failure getting page \'{0}\': Invalid endpoint'.format(url))

            j = json.loads(result.decode('utf-8'))

            if not isinstance(j, dict) or 'reward' not in j or not isinstance(j['reward'], numbers.Number) or \
                    ('children' in j and not isinstance(j, list) \
                    and not all(isinstance(child, str) for child in j['children'])):
                raise ApiTraverserInvalidResponse('API \'{0}\' returned an invalid response.'.format(url))

            returnValue(j)
        except json.JSONDecodeError as e:
            raise ApiTraverserError('Could not decode response from \'{0}\' as JSON.'.format(url)) from e
        except ApiTraverserRequestFailed as e:
            if self.__class__.failed_reward is not None:
                returnValue({'reward': self.failed_reward})
            else:
                raise e


    def _calculate_reward(self, results, reward):
        """Consumes the result of a deferred list crawling the children of the given URL,
        summing their rewards along with the reward at that URL, and returns a success
        deferred containing it. All rewards are assumed to be numeric and are casted to
        ``float``."""

        sum_reward = float(reward)
        for result in results:
            success, reward = result
            if not success: return defer.fail()
            sum_reward += reward
        return sum_reward


    def _ratelimit(self, f, *args, **kwargs):
        """Enter a function into the rate limiting of the bucket defined in this class.
        The function may return a concrete value or a deferred. If the bucket is full,
        the task is retried around the soonest time that the bucket may be empty again."""

        if self.bucket.drip():
            self.bucket.add(self.bucket_add_per_request)
            return defer.maybeDeferred(f, *args, **kwargs)
        else:
            delay = self.bucket.content / self.bucket.rate
            return task.deferLater(self._reactor, delay, self._ratelimit, f, *args, **kwargs)


    def _read_cache(self, url, cache):
        if isinstance(cache[url], defer.DeferredLock):
            d = cache[url].acquire()
            def wait(lock, url):
                lock.release()
                return cache[url]
            d.addCallback(wait, url)
            return d
        else:
            return defer.succeed(cache[url])


    def _write_cache(self, result, url, cache):
        lock = cache[url]
        cache[url] = result
        lock.release()
        return result


    def _init_cache(self, url, cache):
        assert(url not in cache)
        cache[url] = defer.DeferredLock()
        lock = cache[url].acquire()


    # `cache` is the cache data structure, used for both caching
    #   and tree enforcement. Keys are API URL's, and values are either
    #   reward values as ``float`` or ``DeferredLock`` instances which
    #   will be released once the entry has been overwritten with a ``float``.
    #   This allows a traversal on a non-tree to avoid traversing a subgraph
    #   more than once. If caching is disabled, but ``enforce_tree``  is set,
    #   the cache will still be maintained but only the keys are used.
    # `shared_cache` follows the exact same operation but can be shared by
    #   concurrent invocations of ``traverse``.
    def _crawler(self, j, url, cache, use_cache, enforce_tree, shared_cache):
        """Given a dict ``j`` of the expected JSON API format {'reward':..., 'children', [...]},
        and the ``url`` it was obtained from, recursively crawl all descendants, producing a
        deferred which will fire with the sum of all rewards at and below ``url`` in the traversal.
        See ``ApiTraverser.traverse`` for an explanation of options."""

        if enforce_tree and url in cache:
            raise ApiTraverserNonTreeException('API graph is not a tree: detected at \'{0}\''.format(url))
        if use_cache and shared_cache is not None and url in shared_cache:
            return self._read_cache(url, shared_cache)
        elif use_cache and url in cache and cache[url]:
            return self._read_cache(url, cache)
        else:
            if use_cache and shared_cache is not None:
                self._init_cache(url, shared_cache)
            if use_cache or enforce_tree:
                self._init_cache(url, cache)

            deferreds = []
            requests = {}
            children = j.get('children', [])
            for child_url in (set(children) if use_cache else children):
                if not use_cache or child_url not in requests:
                    if use_cache and shared_cache is not None and child_url in shared_cache:
                        d = self._read_cache(child_url, shared_cache)
                    elif use_cache and child_url in cache:
                        d = self._read_cache(child_url, cache)
                    else:
                        d = self._ratelimit(self._crawl, child_url)
                    d.addCallback(self._crawler, child_url, cache, use_cache, enforce_tree, shared_cache)
                    if use_cache: d.addCallback(lambda result: result * children.count(child_url))
                    if use_cache: requests[child_url] = d
                if use_cache:
                    deferreds.append(requests[child_url])
                else:
                    deferreds.append(d)

            dl = defer.DeferredList(deferreds, fireOnOneErrback=True, consumeErrors=True)
            def unwrapFirstError(failure):
                failure.trap(defer.FirstError)
                return failure.value.subFailure
            dl.addErrback(unwrapFirstError)
            dl.addCallback(self._calculate_reward, j['reward'])

            if use_cache and shared_cache is not None:
                dl.addBoth(self._write_cache, url, shared_cache)
            if use_cache or enforce_tree:
                dl.addBoth(self._write_cache, url, cache)

            dl.addErrback(self._failed)
            return dl


    def traverse(self, url, use_cache=True, enforce_tree=True, shared_cache=None):
        """Crawl the API tree and return a deferred containing the sum of rewards.
        ``traverse`` supports concurrent traversals with an optional shared cache.
        HTTP connections share the same ``twisted.web.client.HTTPConnectionPool``.
        API requests are collectively rate-limited which can be configured at
        the class instance level (see ``ApiTraverser`` class attributes).

        Args:
            url (str): Start the traversal at this URL
            use_cache (bool): Caches the result of reward caculation at a
                particular API node. This is most useful when-- in addition to the
                APIs always returning the same results-- nodes have duplicate
                children or when doing a traversal on an API which forms a DAG
                (implying overlapping sub-traversals). Note that a separate cache
                is kept for each call to ``traverse`` unless a shared cache is
                passed in (see ``shared_cache``).
            enforce_tree (bool): Raises an exception when an API is visited twice.
                If ``use_cache`` is not enabled, tree enforcement may be triggered
                by duplicate children of the same node; therefore combining
                ``use_cache`` = False and ``enforce_tree`` = True with API's having
                duplicate children is likely not what you want. When using with
                ``shared_cache`` and concurrent traversal, operation is not
                reliable. Specifically, false negatives and false positives are
                both possible along with incorrect reward results.
            shared_cache (dict): When ``use_cache`` is enabled, passing the same
                dict to multiple invocations to ``traverse`` will allow concurrent
                traversals to share the work of common sub-traversals. Operation is
                the same as the default per-traversal cache enabled by setting
                ``use_cache``, except that concurrent traversals can will share
                information. That is, if a particular sub-traversal is already in
                progress within one invocation of ``traverse``, other concurrent
                invocations will asynchronously consume the result upon completion
                rather than repeating the work.

        Returns:
            Deferred (float): Returns a deferred which will callback with a float
                that is the sum of the 'reward' JSON attributes of the responses
                along the API traversal.

        Raises:
            ApiTraverserError
            ApiTraverserRequestFailed
            ApiTraverserNonTreeException
        """

        start_url = url
        d = self._crawler({'children': [start_url], 'reward': 0}, 'root:'+start_url, {}, use_cache, enforce_tree, shared_cache)

        @inlineCallbacks
        def finish(result):
            yield self.http_pool.closeCachedConnections()
            returnValue(result)
        d.addBoth(finish)

        return d

