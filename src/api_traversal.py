import threading
import sys
from twisted.internet import reactor, defer
from twisted.internet.threads import blockingCallFromThread
from .api_traverser import ApiTraverser
import functools


def apply(arg):
    """A blocking `ApiTraverser` endpoint for Algorithmia. ``arg`` can be a single
    starting URL as string, or a list of starting URLs. In the latter case, the
    traversals will be run concurrently and sharing work when they encounter the
    same node."""

    if isinstance(arg, str):
        url = arg

        def do_traversal(url):
            traverser = ApiTraverser(reactor=reactor)
            d = traverser.traverse(url)
            return d

        f = functools.partial(do_traversal, url)
    elif isinstance(arg, list) and all(isinstance(elem, str) for elem in arg):
        urls = arg

        def do_traversals(urls):
            traverser = ApiTraverser(reactor=reactor)
            shared_cache = {}
            deferreds = []
            for url in urls:
                d = traverser.traverse(url, shared_cache=shared_cache)
                deferreds.append(d)
            dl = defer.DeferredList(deferreds, fireOnOneErrback=True, consumeErrors=True)
            dl.addCallback(lambda results: [r[1] for r in results])
            return dl

        f = functools.partial(do_traversals, urls)
    else:
        raise ValueError('Input must be of type \'str\' or list of \'str\': got \'{0}\''.format(type(arg)))

    try:
        return blockingCallFromThread(reactor, f)
    except Exception as e:
        import traceback
        tb = traceback.format_exc()
        if isinstance(tb, bytes): tb = tb.decode('utf-8')
        return {'error': str(e), 'stacktrace': tb}


# run twisted reactor as a daemon upon importing this module
def run_reactor():
    reactor.run(installSignalHandlers=False)
t = threading.Thread(target=run_reactor)
t.daemon = True
t.start()


# run locally from command line using e.g. `python -m src.api_traversal http://algo.work/interview/a`
if __name__ == '__main__':
    print('total reward: {0}'.format(apply(sys.argv[1])))
