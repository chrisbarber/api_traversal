import pytest
from twisted.trial import unittest
from twisted.test.proto_helpers import MemoryReactorClock
from twisted.internet import task
from twisted.internet.defer import inlineCallbacks
from parameterized import parameterized
from unittest.mock import patch, Mock
import itertools
import functools
import numpy as np
np.random.seed(0)
import json

from ..src.api_traverser import ApiTraverser, ApiTraverserError, \
    ApiTraverserRequestFailed, ApiTraverserNonTreeException, \
    ApiTraverserInvalidResponse


def calculate_reward(api_graph, url, cache, use_cache=True, enforce_tree=True):
    """Simple DFS implementation of the reward calculation on an api graph test case."""

    cache[url] = None
    j = json.loads(api_graph[url]['response'].decode('utf-8'))
    total_reward = float(j['reward'])
    children = j.get('children', [])
    for child in (set(children) if use_cache else children):
        if child in cache and enforce_tree:
            raise ValueError('Not a tree')
        elif use_cache and child in cache and cache[child] is not None:
            reward = cache[child]
        else:
            reward = calculate_reward(api_graph, child, cache, use_cache=use_cache, enforce_tree=enforce_tree)
        if use_cache:
            reward *= children.count(child)
        total_reward += reward
    cache[url] = total_reward
    return total_reward


def random_api_dags(nodes, n):
    """Generate a random api_graph that forms a DAG."""

    for _ in range(n):
        adj=np.tril(np.random.random((len(nodes),)*2)<.5, k=-1)
        yield {url: {'delay': .1, 'response': bytes('{"children":['+','.join(map(lambda s: '"{0}"'.format(s), children))+'],"reward":1}', 'utf-8')}
                for url, children in zip(nodes, [np.array(list(nodes))[adj[i,:]] for i in range(adj.shape[0])])}


class ApiTraverserTest(unittest.TestCase):


    def run_to_finish(self, mem_reactor_clock, resolution=1e-4):
        """Takes a twisted.test.proto_helpers.MemoryReactorClock and runs it at the given
        timestep (``resolution``) until no more delayed calls are present on the reactor.
        Using a fake reactor isolates tests from external issues that may be encountered
        with a real reactor. The tests will also run much faster, with the caveat that
        some experimentation is necessary to choose a low enough resolution to produce
        realistic results."""

        start = mem_reactor_clock.seconds()
        while mem_reactor_clock.getDelayedCalls():
            mem_reactor_clock.advance(resolution)
        elapsed = mem_reactor_clock.seconds() - start
        return elapsed


    def traverse(self, start_url, use_cache=True, enforce_tree=True, shared_cache=None, reactor=None):
        """Convenience method to run a single traversal without manually constructing an ApiTraverser."""

        if reactor is None:
            reactor = MemoryReactorClock()
        traverser = ApiTraverser(reactor=reactor)
        d = traverser.traverse(start_url, use_cache=use_cache, enforce_tree=enforce_tree, shared_cache=shared_cache)
        elapsed = self.run_to_finish(reactor)
        return d, elapsed


    def mock_get_page(self, api_graph, url, reactor):
        """Function used to mock ApiTraverser._get_page which simply returns ``bytes``
        JSON results after a specified delay from a {url: {'response':...,'delay'...}}
        specification of an ``api_graph``."""

        api = api_graph[url]() if isinstance(api_graph[url], Mock) else api_graph[url]
        return task.deferLater(reactor, api['delay'], lambda: api['response'])


    @parameterized.expand(
        itertools.chain(
            # scenario 1
            ((*scenario, *options, *expected) for (scenario, expected), options in
                itertools.product(
                    zip(
                        # scenario
                        zip(
                            itertools.repeat({
                                'a': {'delay': .2, 'response': b'{"children":["b","c"],"reward":1}'},
                                'b': {'delay': 6.6, 'response': b'{"children":["d","e"],"reward":2}'},
                                'c': {'delay': 1., 'response': b'{"children":["f","g"],"reward":3}'},
                                'd': {'delay': .4, 'response': b'{"reward":4}'},
                                'e': {'delay': .6, 'response': b'{"reward":5}'},
                                'f': {'delay': 1., 'response': b'{"children":["h"],"reward":6}'},
                                'g': {'delay': 1.8, 'response': b'{"children":["i","i","i","i","i","i","i","i"],"reward":7}'},
                                'h': {'delay': .2, 'response': b'{"reward":-1}'},
                                'i': {'delay': .2, 'response': b'{"children":["j","j","j","j","j","j","j","j"],"reward":0}'},
                                'j': {'delay': .2, 'response': b'{"children":["k","k","k","k","k","k","k","k"],"reward":0}'},
                                'k': {'delay': .2, 'response': b'{"reward":0.25}'}
                            }),
                            [c for c in 'abcdefghijk']
                        ),
                        # expected
                        zip(
                            itertools.repeat(8.)
                        )
                    ),
                    # options
                    zip(
                        # obey warning and do not use use_cache=False and enforce_tree=True with an API graph
                        # having duplicate children
                        [False,True,True],
                        [False,False,True]
                    )
                )
            )#, add cases here
        )
    )
    def test_traverse_tree(self, api_graph, start_url, use_cache, enforce_tree, maxtime):
        """Test correctness and elapsed time of reward calculation for scenarios."""

        with patch('api_traversal.src.api_traverser.ApiTraverser._get_page') as mock_get_page:
            test_reactor = MemoryReactorClock()
            mock_get_page.side_effect = functools.partial(self.mock_get_page, api_graph, reactor=test_reactor)

            d, elapsed = self.traverse(start_url, use_cache=use_cache, enforce_tree=enforce_tree, reactor=test_reactor)

            expected_reward = calculate_reward(api_graph, start_url, {}, use_cache=use_cache, enforce_tree=enforce_tree)
            self.assertEquals(expected_reward, self.successResultOf(d))
            self.assertTrue(elapsed < maxtime)


    @parameterized.expand(
        itertools.chain(
            ((api_graph, start_url, use_cache, enforce_tree, enforce_tree) for
                    api_graph, start_url, (use_cache, enforce_tree) in
                zip(
                    itertools.repeat({
                        'a': {'delay': .2, 'response': b'{"children":["b","c"],"reward":1}'},
                        'b': {'delay': 6.6, 'response': b'{"children":["d","d"],"reward":2}'},
                        'c': {'delay': 1., 'response': b'{"children":["d"],"reward":3}'},
                        'd': {'delay': .4, 'response': b'{"reward":4}'}
                    }),
                    itertools.repeat('a'),
                    itertools.product(
                        [False,True],
                        [False,True]
                    )
                )
            ),
            ((api_graph, start_url, use_cache, enforce_tree, not use_cache and enforce_tree) for
                    api_graph, start_url, (use_cache, enforce_tree) in
                zip(
                    itertools.repeat({
                        'a': {'delay': .2, 'response': b'{"children":["b","c"],"reward":1}'},
                        'b': {'delay': 6.6, 'response': b'{"children":["d","d","d","d","d","d","d","d","d","d","d","d","d","d","d","d","d","d","d"],"reward":2}'},
                        'c': {'delay': 1., 'response': b'{"reward":3}'},
                        'd': {'delay': .4, 'response': b'{"children":["e","e"],"reward":4}'},
                        'e': {'delay': 1., 'response': b'{"reward":1}'},
                    }),
                    itertools.repeat('a'),
                    itertools.product(
                        [False,True],
                        [False,True]
                    )
                )
            )#, add scenarios here
        )
    )
    def test_enforce_tree(self, api_graph, start_url, use_cache, enforce_tree, nontree):
        """Test non-tree condition."""

        with patch('api_traversal.src.api_traverser.ApiTraverser._get_page') as mock_get_page:
            test_reactor = MemoryReactorClock()
            mock_get_page.side_effect = functools.partial(self.mock_get_page, api_graph, reactor=test_reactor)

            d, _ = self.traverse(start_url, use_cache=use_cache, enforce_tree=enforce_tree, reactor=test_reactor)

            if nontree:
                self.failureResultOf(d, ApiTraverserNonTreeException)
            else:
                self.successResultOf(d)


    @parameterized.expand([
        (None, 11.1, 22.2, list('abdecef'), 3.5),
        ({}, 11.2, 22.2, list('abdec'), 2.5)
    ])
    def test_concurrent(self, shared_cache, reward_a, reward_b, call_order, expected_time):
        """Verify multiple traversals are executed concurrently. Verify concurrent use of the shared cache."""

        api_graph = {
            'a': Mock(return_value={'delay': 2., 'response': b'{"children":["c"],"reward":10}'}),
            'b': Mock(return_value={'delay': 1., 'response': b'{"children":["d"],"reward":20}'}),
            'c': Mock(return_value={'delay': .5, 'response': b'{"children":["e"],"reward":1}'}),
            'd': Mock(return_value={'delay': .5, 'response': b'{"children":["e"],"reward":2}'}),
            'e': Mock(side_effect=[{'delay': 0., 'response': b'{"reward":0.2}'},
                                   {'delay': 1., 'response': b'{"children":["f"],"reward":0.1}'}]),
            'f': Mock(return_value={'delay': 0., 'response': b'{"reward":0}'})
        }

        with patch('api_traversal.src.api_traverser.ApiTraverser._get_page') as mock_get_page:
            test_reactor = MemoryReactorClock()
            mock_get_page.side_effect = functools.partial(self.mock_get_page, api_graph, reactor=test_reactor)

            traverser = ApiTraverser(reactor=test_reactor)
            d1 = traverser.traverse('a', shared_cache=shared_cache)
            d2 = traverser.traverse('b', shared_cache=shared_cache)
            elapsed = self.run_to_finish(test_reactor)

            self.assertEqual(call_order, [c[1][0] for c in mock_get_page.mock_calls])
            self.assertEqual(reward_a, self.successResultOf(d1))
            self.assertEqual(reward_b, self.successResultOf(d2))
            print(elapsed, expected_time)
            self.assertTrue(abs(elapsed - expected_time) < .5)


    def test_enforce_tree_concurrent(self):
        """Verify that concurrent traversals sharing nodes does not trigger a false-
        positive ApiTraversalNonTreeException."""

        api_graph = {
            'a': {'delay': 2., 'response': b'{"children":["b"],"reward":10}'},
            'b': {'delay': 1., 'response': b'{"children":["c"],"reward":20}'},
            'c': {'delay': .5, 'response': b'{"children":["d"],"reward":1}'},
            'd': {'delay': .5, 'response': b'{"children":["e"],"reward":2}'},
            'e': {'delay': 1., 'response': b'{"children":["f"],"reward":0.2}'},
            'f': {'delay': 2., 'response': b'{"children":["a","g"],"reward":0}'},
            'g': {'delay': 2., 'response': b'{"children":["h"],"reward":0.2}'},
            'h': {'delay': 1., 'response': b'{"children":["i"],"reward":0.2}'},
            'i': {'delay': 2., 'response': b'{"children":["j"],"reward":0.2}'},
            'j': {'delay': 3., 'response': b'{"children":["k"],"reward":0.2}'},
            'k': {'delay': .5, 'response': b'{"children":["l"],"reward":0.2}'},
            'l': {'delay': 1., 'response': b'{"children":[],"reward":0.2}'}
        }

        with patch('api_traversal.src.api_traverser.ApiTraverser._get_page') as mock_get_page:
            test_reactor = MemoryReactorClock()
            mock_get_page.side_effect = functools.partial(self.mock_get_page, api_graph, reactor=test_reactor)

            shared_cache = {}
            traverser = ApiTraverser(reactor=test_reactor)
            d1 = traverser.traverse('a', shared_cache=shared_cache)
            d2 = traverser.traverse('g', shared_cache=shared_cache)
            elapsed = self.run_to_finish(test_reactor)

            self.assertNoResult(d1)
            self.assertEqual(1.2, self.successResultOf(d2))


    @parameterized.expand(zip([False,True]))
    def test_cycle(self, use_cache):
        """Assert that a cycle in the API graph will cause the traversal to run indefinitely
        when not using ``enforce_tree``."""

        api_graph = {
            'a': {'delay': 2., 'response': b'{"children":["b"],"reward":10}'},
            'b': {'delay': 1., 'response': b'{"children":["c"],"reward":20}'},
            'c': {'delay': .5, 'response': b'{"children":["d"],"reward":1}'},
            'd': {'delay': .5, 'response': b'{"children":["e"],"reward":2}'},
            'e': {'delay': 1., 'response': b'{"children":["f"],"reward":0.2}'},
            'f': {'delay': 2., 'response': b'{"children":["a"],"reward":0}'}
        }

        with patch('api_traversal.src.api_traverser.ApiTraverser._get_page') as mock_get_page:
            test_reactor = MemoryReactorClock()
            mock_get_page.side_effect = functools.partial(self.mock_get_page, api_graph, reactor=test_reactor)

            traverser = ApiTraverser(reactor=test_reactor)
            d = traverser.traverse('a', use_cache=use_cache, enforce_tree=False)
            test_reactor.pump(itertools.repeat(1e-3, times=10**6))
            self.assertNoResult(d)


    def test_failed_reward(self):
        """Test that the failed reward class attribute can be set and is used correctly in
        calculating total reward."""

        api_graph = {
            'a': {'delay': 2., 'response': b'{"children":["b"],"reward":10}'},
            'b': {'delay': 1., 'response': b'{"children":["c"],"reward":20}'},
            'c': {'delay': .5, 'response': b'{"children":["d"],"reward":1}'},
            'd': {'delay': .5, 'response': b'{"children":["e"],"reward":2}'},
            'e': {'delay': 1., 'response': b'{"children":["f"],"reward":0.2}'},
            'f': {'delay': 2., 'response': b'{"children":["g"],"reward":0.1}'} # "g" does not exist
        }

        with patch('api_traversal.src.api_traverser.ApiTraverser._get_page') as mock_get_page:
            test_reactor = MemoryReactorClock()
            def get_page(url):
                from ..src.api_traverser import ApiTraverserRequestFailed
                if url == 'g':
                    raise ApiTraverserRequestFailed()
                else:
                    return functools.partial(self.mock_get_page, api_graph, reactor=test_reactor)(url)
            mock_get_page.side_effect = get_page

            class CustomApiTraverser(ApiTraverser):
                failed_reward = 300.
            traverser = CustomApiTraverser(reactor=test_reactor)
            d = traverser.traverse('a', use_cache=False, enforce_tree=False)
            self.run_to_finish(test_reactor)
            self.assertEqual(333.3, self.successResultOf(d))


    @parameterized.expand((*scenario, *options) for scenario, options in
        itertools.product(
            zip(
                random_api_dags([str(i) for i in range(1,11)], 10),
                [str(np.random.randint(1, 10)) for i in range(10)],
                [str(np.random.randint(1, 10)) for i in range(10)]
            ),
            # options
            itertools.product(
                [False,True],
                [False,True],
                [False,True]
            )
        )
    )
    def test_random_dag_concurrent(self, api_graph, start_url, second_start_url, use_cache, enforce_tree, use_shared_cache):
        """ Test two concurrent traversals on a random API graph that forms a DAG. There is
        just one "gotcha" in this scenario, which is that ``enforce_tree`` is expected to
        behave unreliably (false negatives and false positives) when using a shared cache."""

        with patch('api_traversal.src.api_traverser.ApiTraverser._get_page') as mock_get_page:
            test_reactor = MemoryReactorClock()

            mock_get_page.side_effect = functools.partial(self.mock_get_page, api_graph, reactor=test_reactor)
            if use_shared_cache:
                shared_cache = {}
                enforce_tree = False # enforce_tree behavior is unpredictable when using shared_cache with non-tree api graphs
            else:
                shared_cache = None
            d, _ = self.traverse(start_url, use_cache=use_cache, enforce_tree=enforce_tree, shared_cache=shared_cache, reactor=test_reactor)
            d2, _ = self.traverse(second_start_url, use_cache=use_cache, enforce_tree=enforce_tree, shared_cache=shared_cache, reactor=test_reactor)

            try:
                expected_reward = calculate_reward(api_graph, start_url, {}, use_cache=use_cache, enforce_tree=enforce_tree)
            except:
                expected_reward = None
            try:
                second_expected_reward = calculate_reward(api_graph, second_start_url, {}, use_cache=use_cache, enforce_tree=enforce_tree)
            except:
                second_expected_reward = None
            if expected_reward is None:
                self.failureResultOf(d, ApiTraverserNonTreeException)
            else:
                self.assertEquals(expected_reward, self.successResultOf(d))
            if second_expected_reward is None:
                self.failureResultOf(d2, ApiTraverserNonTreeException)
            else:
                self.assertEquals(second_expected_reward, self.successResultOf(d2))


    @parameterized.expand([
        ""
        "junk",
        "junk://",
        "junk://junk.com"
    ])
    @inlineCallbacks
    def test_traverse_bad_start_url(self, start_url):
        """Some test cases with bad start URLs."""

        with self.assertRaises(ApiTraverserRequestFailed):
            d, elapsed = self.traverse(start_url)
            self.assertEqual(0., elapsed)
            yield d


    @parameterized.expand(zip([
        b'{}',
        b'{"children":[]}',
        b'{"reward":"junk","children":[]}',
        b'{"reward":1,"children":[1,2,3]}'
    ]))
    def test_bad_response(self, response):
        """Test that an api returning an unexpected response format will raise an error."""

        with patch('api_traversal.src.api_traverser.ApiTraverser._get_page') as mock_get_page:
            test_reactor = MemoryReactorClock()

            mock_get_page.return_value = response
            d, _ = self.traverse('junk', reactor=test_reactor)
            self.failureResultOf(d, ApiTraverserInvalidResponse)
