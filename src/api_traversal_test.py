import unittest
from mock import patch
import urllib.request
from parameterized import parameterized
import itertools
import random
random.seed(0)

from ..src import api_traversal


def api_available():
    try:
        return 200 == urllib.request.urlopen('http://algo.work/interview/a', timeout=2.).getcode()
    except:
        return False

@unittest.skipIf(not api_available(), 'algo.work APIs unavailable')
class ApiTraversalTest(unittest.TestCase):

    def test_api_traversal(self):
        self.assertEqual(155.,  api_traversal.apply("http://algo.work/interview/a"))


    def random_permutation(iterable, r=None):
        """Random selection from itertools.permutations(iterable, r)"""

        pool = tuple(iterable)
        r = len(pool) if r is None else r
        yield tuple(random.sample(pool, r))
    @parameterized.expand(
        itertools.repeat(''.join(next(random_permutation('abcdefghijk', r=random.randint(1,11)))), times=10)
    )
    def test_api_traversal_multiple(self, order):
        """Test that concurrent traversals starting at a random subset of nodes and initiated
        in random order produce correct results."""

        start_urls = ['http://algo.work/interview/'+c for c in order]
        expected = [155.,11.,143.,4.,5.,5.,135.,-1.,16.,2.,.25]
        self.assertEqual([expected['abcdefghijk'.index(start_url)] for start_url in order], api_traversal.apply(start_urls))


    def test_api_traversal_failures(self):
        self.assertTrue("error" in api_traversal.apply(""))
        self.assertTrue("error" in api_traversal.apply("ftp://algo.work/interview/a"))
        self.assertTrue("error" in api_traversal.apply("http://www.example.com"))
        self.assertTrue("error" in api_traversal.apply("http://1234"))
        self.assertTrue("error" in api_traversal.apply("http://z"))
        self.assertTrue("error" in api_traversal.apply("http://algo.work:10/interview/a")) # will time out
        self.assertTrue("error" in api_traversal.apply("http://algo.work/interview/junk"))
        self.assertTrue("error" in api_traversal.apply(["", "http://algo.work/interview/a"]))

