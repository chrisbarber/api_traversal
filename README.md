## Overview

Traverse a tree of APIs set up like:

```
GET http://algo.work/interview/a
```

Each endpoint returns JSON of the form:

```
{"children":["http://algo.work/interview/b","http://algo.work/interview/c"],"reward":1}
```

This algorithm assumes that the APIs always return the same result, enabling concurrent traversals that do not duplicate effort.

## Usage

### Input

Input is given as either a single start URL:

```
"http://algo.work/interview/a"
```

Or a list of start URLs to traverse concurrently:

```
["http://algo.work/interview/a", "http://algo.work/interview/c"]
```

### Output

If a single start URL is given, the result is a numeric sum of rewards found in the traversal. If multiple URLs are given, the result is a list of sums of rewards of traversals performed at start URLs in the same order that they were given.

Input:
```
"http://algo.work/interview/a"
```
Output:
```
155
```
Input:
```
["http://algo.work/interview/a",
 "http://algo.work/interview/b",
 "http://algo.work/interview/c",
 "http://algo.work/interview/d",
 "http://algo.work/interview/e",
 "http://algo.work/interview/f",
 "http://algo.work/interview/g",
 "http://algo.work/interview/h",
 "http://algo.work/interview/i",
 "http://algo.work/interview/j",
 "http://algo.work/interview/k"]
 ```
 Output:
 ```
 [155, 11, 143, 4, 5, 5, 135, -1, 16, 2, 0.25]
 ```

### Exceptions

#### Non-tree conditions

When called with a single `str` input, the algorithm is guaranteed to correctly return in error indicating if the APIs do not form a tree:

```
{"error":"API graph is not a tree: detected at 'http://some.url/api'", "stacktrace": ...}
```

**When called with a `list` input (concurrent/shared traversal) and the APIs do not form a tree, the algorithm tries to detect this and will return an error if it does, but is not guaranteed to be able to do so**. Expect the possibility of incorrect reward results being returned in such cases. Even so, the algorithm is guaranteed to halt even if the APIs contain a cycle.

#### API failure conditions

The algorithm will return in error if an endpoint is inaccessible for some reason, does not return valid JSON, or does not return the expected JSON format.

```
{"error":"Failure getting page 'http://some.url/api'", "stacktrace": ...}
```

```
{"error":"Could not decode response from 'http://some.url/api' as JSON.", "stacktrace": ...}
```

```
{"error":"API 'http://some.url/api' returned an invalid response.", "stacktrace": ...}
```